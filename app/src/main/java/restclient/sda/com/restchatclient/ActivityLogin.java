package restclient.sda.com.restchatclient;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import restclient.sda.com.restchatclient.Util.SessionManager;
import restclient.sda.com.restchatclient.clients.RestClientRegister;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class ActivityLogin extends AppCompatActivity {
    @BindView (R.id.login)
    protected EditText login;
    @BindView (R.id.password)
    protected EditText password ;
    @BindView (R.id.sign_in_button)
    protected Button sign_in_button ;
    @OnClick (R.id.sign_in_button)
    protected void signInOrRegister () {
        JSONObject response = RestClientRegister. invokeRegister(
                login.getText().toString() ,
                password .getText().toString()) ;
        try {
            String responseResult = response.get( "result" ).toString() ;
            if (responseResult.equals( "SUCCESS") || responseResult.equals( "REGISTERED" )){
                Intent i = new Intent(getApplicationContext() , UserListActivity. class);

                SessionManager.getInstance().setToken(
                        Integer.parseInt(response.get( "id" ).toString()),
                        response.get( "token" ).toString());

                startActivity(i) ;
            }
        } catch (Exception e){
            e.printStackTrace() ;
        }
    }

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState) ;
        setContentView(R.layout. activity_login);
        ButterKnife. bind(this);
    }
}

