package restclient.sda.com.restchatclient;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import restclient.sda.com.restchatclient.Util.SessionManager;
import restclient.sda.com.restchatclient.clients.RestClientMsgRetriever;
import restclient.sda.com.restchatclient.clients.RestClientMsgSender;
import restclient.sda.com.restchatclient.clients.RestClientMsgUpdater;
import restclient.sda.com.restchatclient.model.Message;

public class ConversationActivity extends AppCompatActivity {

    @BindView(R.id.listView)
    protected RecyclerView listView;

    @BindView(R.id.editText)
    protected EditText editText;

    @OnClick(R.id.buttonSend)
    protected void send() {
        JSONObject obj = RestClientMsgSender.sendMessage(
                SessionManager.getInstance().getUserId(),
                recipient_id,
                editText.getText().toString());
    }

    private MessageAdapter msgAdapter;
    private int recipient_id;

    private Handler handler = new Handler();
    private Runnable periodicTask = new Runnable() {
        @Override
        public void run() {
//            Toast.makeText(getApplicationContext(), "Czek", Toast.LENGTH_SHORT).show();
            checkMessages();
            handler.postDelayed(periodicTask, 5000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        ButterKnife.bind(this);

        recipient_id = getIntent().getIntExtra("recipient_id", -1);

        listView.setHasFixedSize(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        msgAdapter = new MessageAdapter(new LinkedList<Message>());
        listView.setLayoutManager(layoutManager);
        listView.setAdapter(msgAdapter);

        getAllMessages();
    }

    @Override
    protected void onResume() {
        handler.postDelayed(periodicTask, 5000);
        super.onResume();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(periodicTask);
        super.onPause();
    }

    private void checkMessages() {
        JSONObject result = RestClientMsgUpdater.getUnreadMessages(SessionManager.getInstance().getUserId(),
                recipient_id);

        try {
            JSONArray array = result.getJSONArray("result");
            for (int i = 0; i < array.length(); i++) {
                JSONObject message = (JSONObject) array.get(i);

                msgAdapter.addMessage(new Message(
                        message.getInt("sender_id"),
                        message.getInt("recipient_id"),
                        message.getString("content")
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void getAllMessages() {
        JSONObject obj = RestClientMsgRetriever.getMessages(
                SessionManager.getInstance().getUserId(),
                recipient_id);
        try {
            JSONArray arr = obj.getJSONArray("result");
            for (int i = 0; i < arr.length(); i++) {
                JSONObject message = (JSONObject) arr.get(i);

                msgAdapter.addMessage(new Message(
                        message.getInt("sender_id"),
                        message.getInt("recipient_id"),
                        message.getString("content")
                ));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
