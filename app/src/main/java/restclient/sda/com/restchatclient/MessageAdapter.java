package restclient.sda.com.restchatclient;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import restclient.sda.com.restchatclient.Util.SessionManager;
import restclient.sda.com.restchatclient.model.Message;

/**
 * Created by amen on 3/18/17.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageItemHolder> {

    List<Message> messageList;
    private final int USER = 1;

    private final int FRIEND = 2;

    public MessageAdapter() {
    }

    public MessageAdapter(List<Message> messages) {
        this.messageList = messages;
    }

    public void setNewsList(List<Message> messages) {
        this.messageList = messages;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if(messageList.get(position).getSender_id()== SessionManager.getInstance().getUserId()) return USER;
        return FRIEND;
    }

    @Override
    public MessageItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(viewType==USER) view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item_sent, parent, false);
        else view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item_received, parent, false);
        return new MessageItemHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageItemHolder holder, int position) {
        holder.message = messageList.get(position);
        holder.setViews();
    }

    public void addMessage(Message m){
        messageList.add(m);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class MessageItemHolder extends RecyclerView.ViewHolder {

        Message message;
        TextView content;

        public void setViews(){
            content.setText(message.getContent());
        }

        public MessageItemHolder(View itemView) {
            super(itemView);
            content =(TextView) itemView.findViewById(R.id.content);
        }
    }
}