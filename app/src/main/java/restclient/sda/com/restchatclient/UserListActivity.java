package restclient.sda.com.restchatclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import restclient.sda.com.restchatclient.clients.RestClientUserList;
import restclient.sda.com.restchatclient.location.RestClientLocationGetter;
import restclient.sda.com.restchatclient.model.Location;
import restclient.sda.com.restchatclient.model.User;

public class UserListActivity extends AppCompatActivity {
    @BindView(R.id.listView)
    protected ListView userList;

    @OnItemClick(R.id.listView)
    protected void onClick(AdapterView<?> parent, View view, int position, long id) {
        User clickedUser = usersAdapter.getItem(position);
        startConversation(clickedUser.getId());
    }

    @OnItemLongClick(R.id.listView)
    protected boolean showPosition(AdapterView<?> parent, View view, int position, long id){
        User clickedUser = usersAdapter.getItem(position);

        JSONObject object = RestClientLocationGetter.getLocation(clickedUser.getId());
        try {
            JSONObject obj = object.getJSONObject("result");
            Location l = new Location(obj.getDouble("lat"), obj.getDouble("lon"), obj.getString("comment"));
            Toast.makeText(getApplicationContext(), l.toString(), Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    private ArrayAdapter<User> usersAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.set_location:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);

        usersAdapter = new ArrayAdapter<User>(
                getBaseContext(),
                android.R.layout.simple_list_item_1,
                new LinkedList<User>());
        userList.setAdapter(usersAdapter);
        getUserList();
    }

    private void startConversation(int otherUser) {
        Intent i = new Intent(getApplicationContext(), ConversationActivity.class);
        i.putExtra("recipient_id", otherUser);
        startActivity(i);
    }

    private void getUserList() {
        JSONObject obj = RestClientUserList.invokeGetUserList();
        try {
            JSONArray arr = obj.getJSONArray("result");
            for (int i = 0; i < arr.length(); i++) {
                JSONObject objectFromArray = (JSONObject)arr.get(i);

                usersAdapter.add(new User(objectFromArray.getInt("id"),
                        objectFromArray.getString("login")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}