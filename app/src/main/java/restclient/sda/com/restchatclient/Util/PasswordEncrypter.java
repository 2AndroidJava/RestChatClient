package restclient.sda.com.restchatclient.Util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by amen on 3/16/17.
 */

public class PasswordEncrypter {

    public static String encryptString(String toEncrypt){
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(toEncrypt.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


}
