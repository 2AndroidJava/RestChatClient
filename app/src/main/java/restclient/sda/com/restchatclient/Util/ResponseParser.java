package restclient.sda.com.restchatclient.Util;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import restclient.sda.com.restchatclient.ActivityLogin;

/**
 * Created by amen on 3/16/17.
 */

public class ResponseParser {
    public static JSONObject extractJSON (HttpResponse pResp ) {
        JSONObject   pObj  = null;
        try {
            if ( pResp == null ) {
                Log.d( ActivityLogin.class.getName(), "Can't parse - no response." );
                return null;
            }

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader( pResp.getEntity().getContent(), "UTF-8" ) );
            String json = reader.readLine();
            JSONTokener tokener = new JSONTokener( json );
            pObj = new JSONObject( tokener );
        } catch ( JSONException e ) {
            e.printStackTrace();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        return pObj;
    }
}
