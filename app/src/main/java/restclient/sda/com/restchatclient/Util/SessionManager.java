package restclient.sda.com.restchatclient.Util;

/**
 * Created by amen on 3/17/17.
 */
public class SessionManager {
    private static SessionManager ourInstance = new SessionManager();

    public static SessionManager getInstance() {
        return ourInstance;
    }

    private int my_id;
    private String token;

    private SessionManager() {
    }

    public void setToken(int id, String token){
        my_id = id;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public int getUserId() {
        return my_id;
    }
}
