package restclient.sda.com.restchatclient.clients;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import restclient.sda.com.restchatclient.Util.SessionManager;
import restclient.sda.com.restchatclient.model.User;

/**
 * Created by amen on 3/17/17.
 */

public class RestClientMsgSender extends RestClientBase {
    private int sender_id, recipient_id;
    private String content, token;

    private RestClientMsgSender(int sender_id, int recipient_id, String content) {
        super("POST", "http://192.168.1.169:8081/RESTDataProvider/chat/conversation/send");

        this.recipient_id = recipient_id;
        this.sender_id = sender_id;
        this.content = content;
        this.token = SessionManager.getInstance().getToken();
    }

    protected void prepare() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("sender_id", String.valueOf(sender_id));
            obj.put("recipient_id", String.valueOf(recipient_id));
            obj.put("content", content);
            obj.put("token", token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setData(obj);
    }

    public static JSONObject sendMessage(int sender_id, int recipient_id, String content) {
        JSONObject response = null;
        RestClientMsgSender task = null;
        try {
            task = new RestClientMsgSender(sender_id, recipient_id, content);
            task.prepare();
            task.execute();
            response = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return response;
    }
}
