package restclient.sda.com.restchatclient.clients;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import restclient.sda.com.restchatclient.Util.SessionManager;

/**
 * Created by amen on 3/18/17.
 */

public class RestClientMsgUpdater extends RestClientBase {
    private int sender_id, recipient_id;
    private String token;

    private RestClientMsgUpdater(int sender_id, int recipient_id) {
        super("POST", "http://192.168.1.169:8081/RESTDataProvider/chat/conversation/getUnread");

        this.recipient_id = recipient_id;
        this.sender_id = sender_id;
        this.token = SessionManager.getInstance().getToken();
    }

    protected void prepare() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("sender_id", String.valueOf(sender_id));
            obj.put("recipient_id", String.valueOf(recipient_id));
            obj.put("token", token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setData(obj);
    }

    public static JSONObject getUnreadMessages(int sender_id, int recipient_id) {
        JSONObject response = null;
        RestClientMsgUpdater task = null;
        try {
            task = new RestClientMsgUpdater(sender_id, recipient_id);
            task.prepare();
            task.execute();
            response = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return response;
    }
}
