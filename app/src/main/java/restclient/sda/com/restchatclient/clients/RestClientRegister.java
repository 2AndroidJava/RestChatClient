package restclient.sda.com.restchatclient.clients;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import restclient.sda.com.restchatclient.Util.PasswordEncrypter;
import restclient.sda.com.restchatclient.model.User;

/**
 * Created by amen on 3/16/17.
 */

public class RestClientRegister extends RestClientBase {
    private String login;
    private String password_hash ;
    private RestClientRegister (String login , String password){
        super( "POST", "http://192.168.1.169:8081/RESTDataProvider/chat/register" );
        this .login = login;
        this .password_hash = PasswordEncrypter. encryptString(password) ;
    }

    protected void prepare(){
        JSONObject obj = new User(login, password_hash ).toJSONObject() ;
        setData(obj) ;
    }

    public static JSONObject invokeRegister (String login , String password) {
        JSONObject response = null;
        RestClientRegister task = null;
        try {
            task = new RestClientRegister(login , password) ;
            task.prepare() ;
            task.execute() ;
            response = task.get() ;
        } catch (InterruptedException e) {
            e.printStackTrace() ;
        } catch (ExecutionException e) {
            e.printStackTrace() ;
        } catch (NumberFormatException e) {
            e.printStackTrace() ;
        }
        return response ;
    }
}