package restclient.sda.com.restchatclient.clients;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import restclient.sda.com.restchatclient.Util.PasswordEncrypter;
import restclient.sda.com.restchatclient.model.User;

/**
 * Created by RENT on 2017-03-17.
 */

public class RestClientUserList extends RestClientBase {

    private RestClientUserList (){
        super( "GET", "http://192.168.1.169:8081/RESTDataProvider/chat/room" );
    }

    protected void prepare(){

    }

    public static JSONObject invokeGetUserList () {
        JSONObject response = null;
        RestClientUserList task = null;
        try {
            task = new RestClientUserList() ;
            task.prepare() ;
            task.execute() ;
            response = task.get() ;
        } catch (InterruptedException e) {
            e.printStackTrace() ;
        } catch (ExecutionException e) {
            e.printStackTrace() ;
        } catch (NumberFormatException e) {
            e.printStackTrace() ;
        }
        return response ;
    }
}