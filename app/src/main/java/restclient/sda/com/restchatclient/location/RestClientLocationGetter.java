package restclient.sda.com.restchatclient.location;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import restclient.sda.com.restchatclient.Util.SessionManager;
import restclient.sda.com.restchatclient.clients.RestClientBase;

/**
 * Created by amen on 3/18/17.
 */

public class RestClientLocationGetter extends RestClientBase {
    private int user_id;

    private RestClientLocationGetter(int user_id) {
        super("POST", "http://192.168.1.169:8081/RESTDataProvider/location/getLocation");

        this.user_id = user_id;
    }

    protected void prepare() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("user_id", user_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setData(obj);
    }

    public static JSONObject getLocation(int user_id) {
        JSONObject response = null;
        RestClientLocationGetter task = null;
        try {
            task = new RestClientLocationGetter(user_id);
            task.prepare();
            task.execute();
            response = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return response;
    }
}