package restclient.sda.com.restchatclient.location;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import restclient.sda.com.restchatclient.Util.SessionManager;
import restclient.sda.com.restchatclient.clients.RestClientBase;
import restclient.sda.com.restchatclient.clients.RestClientMsgRetriever;

/**
 * Created by amen on 3/18/17.
 */

public class RestClientLocationSetter extends RestClientBase {
    private double lat, lon;
    private int user_id;
    private String comment;

    private RestClientLocationSetter(double lat, double lon, String comment) {
        super("POST", "http://192.168.1.169:8081/RESTDataProvider/location/setLocation");

        this.user_id = SessionManager.getInstance().getUserId();
        this.lat = lat;
        this.lon = lon;
        this.comment = comment;
    }

    protected void prepare() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("user_id", user_id);
            obj.put("lat", lat);
            obj.put("lon", lon);
            obj.put("comment", comment);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setData(obj);
    }

    public static JSONObject setLocation(double lat, double lon, String comment) {
        JSONObject response = null;
        RestClientLocationSetter task = null;
        try {
            task = new RestClientLocationSetter(lat, lon, comment);
            task.prepare();
            task.execute();
            response = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return response;
    }
}