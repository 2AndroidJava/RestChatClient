package restclient.sda.com.restchatclient.model;

/**
 * Created by amen on 3/18/17.
 */

public class Location {
    private double lat;
    private double lon;
    private String comment;

    public Location(double lat, double lon, String comment) {
        this.lat = lat;
        this.lon = lon;
        this.comment = comment;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return lat + " " + lon + " " + comment;
    }
}
