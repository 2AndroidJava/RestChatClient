package restclient.sda.com.restchatclient.model;

import restclient.sda.com.restchatclient.Util.SessionManager;

/**
 * Created by RENT on 2017-03-16.
 */

public class Message {
    private int id;
    private int sender_id;
    private int receiver_id;
    private String content;
    private String timestamp;
    private int status;

    public Message(int sender_id, int receiver_id, String content) {
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public int getSender_id() {
        return sender_id;
    }

    public int getReceiver_id() {
        return receiver_id;
    }

    public String getContent() {
        return content;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        if (SessionManager.getInstance().getUserId() == sender_id) {
            return "You:" + content;
        } else {
            return "" + content;
        }
    }
}
