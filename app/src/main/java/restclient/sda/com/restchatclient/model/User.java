package restclient.sda.com.restchatclient.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by RENT on 2017-03-16.
 */

public class User {
    private int id;
    private String login;
    private String password_hash;

    public User(String login, String password_hash) {
        this.login = login;
        this.password_hash = password_hash;
    }

    public User(int id, String login) {
        this.login = login;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public JSONObject toJSONObject() {
        try {
            JSONObject obj = new JSONObject();

            obj.put("login", login);
            obj.put("password_hash", password_hash);

            return obj;
        } catch (JSONException jne) {

        }
        return null;
    }

    @Override
    public String toString() {
        return id + " - " + login;
    }
}
